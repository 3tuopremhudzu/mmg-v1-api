<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreTaskDefinitionRequest;
use App\Http\Requests\Api\UpdateTaskDefinitionRequest;
use App\Http\Resources\TaskDefinitionResource;
use App\Http\Resources\TaskDefinitionCollection;
use App\Models\TaskDefinition;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class TaskDefinitionApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/task-definition",
     *      operationId="listTaskDefinition",
     *      tags={"TaskDefinition"},
     *      summary="List all task definitions",
     *      description="Returns task definitions data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        return new TaskDefinitionCollection(TaskDefinition::all());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/task-definition",
     *      operationId="storeTaskDefinition",
     *      tags={"TaskDefinition"},
     *      summary="Store new task definition",
     *      description="Returns task definition  data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "category"},
     *              @OA\Property(property="name", type="string", example="Sowing Patatoes"),
     *              @OA\Property(property="category", type="string", example="Sow"),
     *              @OA\Property(property="task_definition_id", type="integer", example="1"), 
     *              @OA\Property(property="description", type="string", example="..."),
     *              @OA\Property(property="date_beg", type="date", example=""),
     *              @OA\Property(property="date_end", type="date", example=""),
     *              @OA\Property(property="plant_definition", type="date", example="")
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreTaskDefinitionRequest $request)
    {
        #TODO : category should be replaced by task_definition_id
        $task_definition = TaskDefinition::create($request->all());

        return (new TaskDefinitionResource($task_definition))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/task-definition/{id}",
     *      operationId="getTaskDefinitionById",
     *      tags={"TaskDefinition"},
     *      summary="Get task definition information",
     *      description="Returns task definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "id": 1, 
     *                      "name": "Sowing potatoes",
     *                      "category": "Sow",
     *                      "task_definition_type": "{id:1, name:'Sow'}",
     *                      "description": "...",
     *                      "date_beg": "2020-12-08",
     *                      "date_end": "2020-12-08",
     *                      "plant_definition": "{id:1, name:'Pommier'}"
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show task definition  $id");
        $task_definition = TaskDefinition::findOrFail($id);
        return (new TaskDefinitionResource($task_definition))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/task-definition/{id}",
     *      operationId="updateTaskDefinition",
     *      tags={"TaskDefinition"},
     *      summary="Update existing task definition",
     *      description="Returns updated task definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "category"},
     *              @OA\Property(property="name", type="string", example="Sowing potatoes"),
     *              @OA\Property(property="category", type="string", example="Sow"),
     *              @OA\Property(property="task_definition_type", type="integer", example="1"), 
     *              @OA\Property(property="description", type="string", example="..."),
     *              @OA\Property(property="date_beg", type="date", example="2020-12-08"),
     *              @OA\Property(property="date_end", type="date", example="2020-12-08"),
     *              @OA\Property(property="plant_definition", type="integer", example="1")
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdateTaskDefinitionRequest $request)
    {
        Log::debug("Udpate task definition $id");
        $task_definition = TaskDefinition::findOrFail($id);
        $task_definition->update($request->all());

        return (new TaskDefinitionResource($task_definition))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/task-definition/{id}",
     *      operationId="deleteTaskDefinition",
     *      tags={"TaskDefinition"},
     *      summary="Delete existing task definition",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        $task_definition = TaskDefinition::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($task_definition)) {
            $task_definition->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}