<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Zone;
use App\Models\Repositories\ZoneRepository;
use App\Models\Repositories\PlantRepository;
use App\Models\Repositories\TaskRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomePageController extends Controller
{
    private $zoneRepository;
    private $plantRepository;
    private $taskRepository;

    public function __construct(ZoneRepository $zoneRepository, PlantRepository $plantRepository,TaskRepository $taskRepository ) {
        $this->zoneRepository = $zoneRepository;
        $this->plantRepository = $plantRepository;
        $this->taskRepository = $taskRepository;
    }
    public function index()
    {

        $user = User::findOrFail(Auth::id());
        $zones = $this->zoneRepository->getByUser($user);
        $plants = $this->plantRepository->getByZone($zones);
        $tasks = $this->taskRepository->getByPlant($plants);
        return Inertia::render('HomePage', compact('user', 'zones', 'plants', 'tasks') );
    }

    public function update(Request $request, $id)
    {

        $zone = Zone::findOrFail($id);
        $zone->update($request->all());
       /* $zone->name = $request->input('name');
        $zone->position_top = $request->input('position_top');
        $zone->position_left = $request->input('position_left');
        $zone->width = $request->input('width');
        $zone->height = $request->input('height');*/

        $zone->save();
        return back();
    }
}
