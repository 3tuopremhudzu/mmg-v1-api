<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionRequest;

class UpdateTaskDefinitionTypeRequest extends TaskDefinitionRequest
{
    use ApiRequestTrait;
}
