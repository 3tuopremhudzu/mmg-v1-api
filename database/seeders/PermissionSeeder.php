<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->insert([
            'name' => 'delete admin role to user',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'give admin role',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'delete admin',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'unblock user',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'block user',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'modify user data',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'modify user garden',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'modify user zone',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'modify user task',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'add task',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'add plant',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'add permission',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'add role',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'change user role',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'add user',
            'guard_name' => 'seeder',

        ]);

        DB::table('permissions')->insert([
            'name' => 'delete user',
            'guard_name' => 'seeder',

        ]);
    }
}
