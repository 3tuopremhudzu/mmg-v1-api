<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     */
    public function run(): void
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'guard_name' => 'seeder',

        ]);

        DB::table('roles')->insert([
            'name' => 'user',
            'guard_name' => 'seeder',

        ]);

        DB::table('roles')->insert([
            'name' => 'super admin',
            'guard_name' => 'seeder',

        ]);

        DB::table('roles')->insert([
            'name' => 'demo user',
            'guard_name' => 'seeder',

        ]);
    }
}
