{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<hr>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('zone') }}"><i class="nav-icon la la-expand"></i> Zones</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant') }}"><i class="nav-icon la la-leaf"></i> Plants</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('task') }}"><i class="nav-icon la la-tasks"></i> Tasks</a></li>
<hr>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant-definition') }}"><i class="nav-icon la la-seedling"></i> Plant definitions</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('task-definition') }}"><i class="nav-icon la la-list"></i> Task definitions</a></li>
<hr>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('task-definition-type') }}"><i class="nav-icon la la-list"></i> Task definition types</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant-family') }}"><i class="nav-icon la la-tree"></i> Plant families</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
